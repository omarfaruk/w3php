<?php 
    $a = array('a'=>'A','b'=>'B','c'=>'C');
    $b = array('a'=>'A','d'=>'B','c'=>'DDD');
    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);

    function myfunction($a,$b)
    {
        if($a === $b)
        {
            return 0;
        }

        return ($a > $b) ? 1 : -1;
    }

    $result = array_diff_ukey($a,$b,'myfunction');
    print_r($result);
    echo "</pre>";
?>