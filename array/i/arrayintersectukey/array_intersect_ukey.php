<?php 
    $a = array('a'=>'A','b'=>'BB','c'=>'C','d'=>'d');
    $b = array('a'=>'a','f'=>'B','m'=>'C','c'=>'d');

    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);

    function myfunction($a,$b){
        if($a === $b){
            return 0;
        }
        return ($a > $b ) ? 1 : -1;
    }

    print_r(array_intersect_ukey($a,$b,"myfunction"));

    echo "good morning";
    echo "</pre>";
?>