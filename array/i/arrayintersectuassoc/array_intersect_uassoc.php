<?php 
    $a = array('a'=>'A','b'=>'B','c'=>'CC','d'=>'DD','f'=>'F');
    $b = array('b'=>'B','c'=>'C','m'=>'A','n'=>'N','f'=>'F','k'=>'K');

    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);

    function myfunction($a,$b){
        if($a === $b){
            return 0;
        }
        return ($a > $b) ? 1 : -1;
    }

    print_r(array_intersect_uassoc($a,$b,"myfunction"));
    echo "</pre>";
?>