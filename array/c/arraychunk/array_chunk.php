<?php

    echo "<pre>";

    $a = array('a','b','c','d','f','g','h','i','j','L','k');
    $b = array(
        array('a'=>'korim','b'=>'rahim'),
        array('c'=>'joshim','d'=>'amin'),
        array('e'=>array('1'=>'abbas','2'=>'kholil'),'f'=>'abul'),
        array('g'=>'gilal','h'=>'helal'),
        array('i'=>'ibrahim','j'=>'japan'),
        array('k'=>'kamal','m'=>'mahim')
    );

    echo "<pre>";

    print_r(array_chunk($b,4));

    echo "</pre>";
    echo "<br>";
    
    print_r($a);

    echo "</pre>";
    echo "<br>";

    echo "<pre>";

    print_r(array_chunk($a,5));

    echo "</pre>";

?>