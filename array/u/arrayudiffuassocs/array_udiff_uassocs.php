<?php 
    $a = array('a'=>'A','b'=>'B','c'=>'c','d'=>'D');
    $b = array('a'=>'A','b'=>'b','c'=>'C','d'=>'D');
    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);
    echo "Final item <br>";
    function myfuncvalue($a,$b)
    {
        if($a === $b) return 0;
        return ($a > $b) ? 1 : -1;
    }
    function myfunckey($a,$b)
    {
        if($a === $b) return 0;
        return ($a > $b) ? 1 : -1;
    }
    print_r(array_udiff_uassoc($a,$b,"myfuncvalue","myfunckey"));
    echo "</pre>";
?>