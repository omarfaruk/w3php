<?php 
    $a = array('a'=>'AA','b'=>'BB','c'=>'CC','d'=>'DD');
    $b = array('a'=>'A','b'=>'BB','c'=>'CC','d'=>'DD');
    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);
    echo "Final item <br>";
    function myfunction($a,$b){
        if($a === $b)
        {
            return 0;
        }
        return ($a > $b) ? 1 :-1;
    }
    print_r(array_udiff_assoc($a,$b,"myfunction"));
    echo "</pre>";

?>