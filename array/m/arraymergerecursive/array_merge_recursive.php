<?php 
    $a = array('a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'F');
    $b = array('a'=>'AA','b'=>'BCB','g'=>'G');

    echo "<pre>";
    echo "main item <br>";
    print_r($a);
    echo "sub item <br>";
    print_r($b);
    echo "merge item <br>";
    print_r(array_merge($a,$b));
    echo "merge recursive item <br>";
    print_r(array_merge_recursive($a,$b));
    echo "</pre>";
?>